---
title: synaestheory.online
background_color: '#222222'
---
This is a collection of random musings, generative art, illustrations, games, and web-based content from an Arts Driven Software Engineer with a digital heart and a brain that wants to understand.

Send inquiries to [seth@synaestheory.online](mailto:seth@synaestheory.online)