[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/synaestheory/blog)

<p style="text-align: center">
  <a href="https://tina.io" style="padding: 10px;">
    <img src="public/static/logos/Logo_Ellipse.svg" width="38" height="38">
  </a>
  <a href="https://www.nextjs.org/" style="padding: 10px;">
    <img src="public/static/logos/next-js.svg" width="38" height="38">
  </a>
  <a href="https://vercel.com/" style="padding: 10px;">
    <img src="public/static/logos/vercel.png" width="38" height="38">
  </a>

</p>
<h1 align="center">
  generative
</h1>

## About

## Quick Setup

#### _Set-up Locally_
```
npm install
npm run develop
```

This will start a dev server, navigate to localhost:3000.

## Project Structure

- Site-level configuration is stored in `data/config.json`. This is editable by Tina when you are on the home page.
- Edit styles within each component or page file within the `<style jsx>` tags.
- Global styles live in the `Meta` component.
- `posts/`contains all your markdown blog posts.
- `static/` is where you images live and will get uploaded.
- `pages` is where you page components live.
- The blog pages are dynamically generated with a `slug` parameter. See the template in `pages/blog/[slug].js`.
- The pages & template are comprised of components from `components`.
- The routes are generated in `next.config.js` with `exportPathMap`
