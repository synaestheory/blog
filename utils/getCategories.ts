import {promises} from 'fs';
import {resolve} from 'path';
import fm from 'front-matter';
const {readdir, readFile} = promises;

export async function getCategories() {
  const dirs = await readdir(resolve('posts'));
  const dirChildren = await Promise.all(dirs.map(dir => readdir(resolve('posts', dir)).then(posts => ({dir, posts: posts.length}))));
  return dirChildren.reduce((categories, category) => {
    if (category.posts > 0) {
      categories.push(category.dir);
    }
    return categories;
  }, [])
}

type Post = {
  title: string;
  date: string;
  hero_image: string;
  external_link: string;
  author: string;
}

export async function getCategory(category: string) {
  const files = await readdir(resolve('posts', category));
  // remove trailing slashes and extensions
  return await Promise.all(files.map(async filename => fm<Post>(await readFile(resolve('posts', category, filename), 'utf-8'))));
}