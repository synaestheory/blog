import Header from "./Header";
import Meta from './Meta'

export default function Layout(props) {
  return (
    <section>
    <Meta 
      siteTitle={props.siteTitle} 
      siteDescription={props.siteDescription} 
    />
    <Header siteTitle={props.siteTitle} categories={props.categories} />
    <main className={props.scroll && "scroll"}>
      {props.children}
    </main>
    <style jsx>
      {`
        section {
          overflow: hidden;
          color: #555;
          display: grid;
          grid-template-columns: 1fr;
          grid-template-rows: auto 1fr;
          grid-template-areas: 
            'header'
            'main';
          height: 100vh;
          min-width: 100vw;
          overscroll-behavior: none;
        }
        main {
          grid-area: main;
        }
        .scroll {
          overflow: auto;
          overscroll-behavior: none;
          -webkit-overflow-scrolling: touch;
        }
      `}
    </style>
  </section>
  );
}