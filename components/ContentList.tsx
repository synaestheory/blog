import Image from "next/image";
import Link from "next/link";

const ContentList = ({category, posts}) => {
  return (
    <>
      <ul className="grid">
        {posts.length >= 1 && posts.sort((a,b) => {
          const aDate = new Date(a.attributes.date);
          const bDate = new Date(b.attributes.date);
          return bDate.getTime() - aDate.getTime();
        }).map(post => (
          <Link
            key={post.attributes.title}
            href={{ pathname: `/${category}/${post.attributes.title}` }}
          >
            <a>
              <li>
                <article className="post">
                  <div className="onexone"></div>
                  <div className="content">
                    <h2 className="title">{post.attributes.title}</h2>
                  </div>
                  <div className={"img"}>
                    <Image layout='fill' src={post.attributes.hero_image} alt={post.attributes.hero_image}></Image>
                  </div>
                </article>
              </li>
            </a>
          </Link>
        ))}
      </ul>
      <style jsx>
        {`
          a {
            height: 100%;
          }
          .post {
            width: 100%;
            overflow: hidden;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: column;
          }
          .post::before {
            background: linear-gradient(to bottom right, hsla(0,0%,0%,0.3), hsla(0,0%,0%,0));
            width: 100%;
            height: 100%;
            content: '';
            z-index: -2;
            position: absolute;
          }
          .img {
            opacity: 0.65;
            transition: 0.2s all ease-in-out;
          }
          .post:hover .img {
            opacity: 1;
          }
          .title::before {
            width:100%;
            height:100%;
            background: radial-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0) 66%);
            transition: 0.2s all ease-in-out;
          }
          .title {
            color: #fff;
          }
          .post:hover .title::before {
            content: '';
            position: absolute;
            top: -1.25rem;
            left: -1.25rem;
            max-width: 100%;
            max-height: 100%;
            border-radius: 100%;
            padding: 1.25rem;
            z-index: -1;
          }

          .grid {
            display: grid;
            grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
            grid-template-rows: repeat(auto-fill, minmax(150px, 1fr));
            grid-gap: 1rem;
            justify-content: space-evenly;
            align-items: space-evenly;
            flex: 1;
            overflow-x: hidden;
            padding: 1rem;
          }
          li {
            opacity: inherit;
            display: flex;
            justify-content: center;
            flex-direction: column;
            margin-bottom: 0;
            overflow: hidden;
            border-radius: 8%;
            border: 4px solid #2f2f2f;
            box-shadow: 1px 1px 6px hsla(0, 0%, 0%, 0.4);
            transition: all 0.2s ease-in-out;
          }
          li:hover {
            border: 1px solid #dfdfdf;
            box-shadow: 2px 2px 10px hsla(0,0%,0%,0.6);
          }
          .onexone {
            padding-bottom: 100%;
          }
          .content {
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            width: 100%;
            height: 100%;
            font-size: 8px;
            z-index: 1;
          }
          .title {
            position: relative;
            margin-bottom: 0.5rem;
            text-align: center;
            z-index:1;
            transition: 0.2s all ease-in-out;
          }
          // .title::before, .title::after {
          //   content:'';
          //   left: -8px;
          //   top: -8px;
          //   width: 100%;
          //   position: absolute;
          //   height: 100%;
          //   border-radius: 8%;
          //   padding: 8px;
          //   text-align: center;
          //   z-index:-1;
          //   background: rgba(0,0,0,0.25);
          // }
          .post:hover .title {
            transform: scale(1.1);
          }
          @media (min-width: 768px) {
            .grid {
              grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
            }
            .content {
              font-size: 12px;
            }
          }
          @media (min-width: 1280px) {
            .grid {
              grid-template-columns: repeat(auto-fill, minmax(375px, 1fr));
            }
            .content {
              font-size: 18px;
            }
          }
        `}
      </style>
    </>
  );
};

export default ContentList;
