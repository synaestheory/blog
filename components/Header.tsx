import Link from "next/link";

export default function Header(props) {
  return (
    <header className="header">
      <Link href="/" passHref>
        <h1 className="rainbow home">{props.siteTitle}</h1>
      </Link>
      <nav
        className="nav"
        role="navigation"
        aria-label="main navigation"
      >
        {props.categories.map(category => {
          return <Link key={category} href={`/${category}`} passHref>
            <h3 className="rainbow">{category}</h3>
          </Link>
        })}

        <Link href="/info" passHref>
          <h3 className="rainbow">info</h3>
        </Link>
      </nav>
      <style jsx>
        {`
          header {
            grid-area: header;
            position: sticky;
            top: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            margin-bottom: 8px;
          }
          .home {
            align-self:center;
            margin: 8px;
            font-size: 3rem;
          }
          nav {
            padding: .5rem 1rem;
            border-bottom: 1px solid #333;
            display: flex;
            width: 100%;
            justify-content: space-around;
            flex-direction: row;
            align-items: center;
            background: linear-gradient(#222, #2a2a2a);
            box-shadow: 0px 2px 4px hsla(0,0%,5%,0.3);
          }
          img {
            margin-bottom: 0;
          }
        `}
      </style>
    </header>
  );
}
