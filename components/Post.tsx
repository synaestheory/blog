import * as React from 'react'

const FullscreenIframe = (props) => {
    return <>
        <iframe src={props.external_link} allow="fullscreen" />
        <style jsx>
        {`
            iframe {
                flex: 1;
                width: 100%;
                height: 100%;
                border: none;
                margin: 0;
            }
        `}
        </style>
    </>
}

export default FullscreenIframe;