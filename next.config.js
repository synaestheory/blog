const withPwa = require('next-pwa');
const WorkerPlugin = require('worker-plugin')

module.exports = withPwa({
  pwa: {
    disable: process.env.NODE_ENV === "development",
    register: true,
    dest: 'public'
  },
  webpack: function(config) {
    config.plugins.push(
      new WorkerPlugin({
        // use "self" as the global object when receiving hot updates.
        globalObject: 'self',
      })    
    )
    config.module.rules.push({
      test: /\.md$/,
      loader: 'frontmatter-markdown-loader',
      options: { mode: ['react-component'] }
    })
    return config
  },
})
