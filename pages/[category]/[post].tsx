import * as React from 'react'

import FullscreenIframe from '../../components/Post'
import Layout from '../../components/Layout'

export default function PostTemplate(props) {
  return (
    <Layout siteTitle={props.title} categories={props.categories}>
      <FullscreenIframe external_link={props.external_link} />
    </Layout>
  )
}

export async function getStaticProps(ctx) {
  const {attributes} = await import(`../../posts/${ctx.params.category}/${ctx.params.post}.md`);
  const categories = await getCategories();
  return {
    props: {
      title: attributes.title,
      external_link: attributes.external_link,
      categories,
    }
  }
}

import { getCategories, getCategory } from '../../utils/getCategories'

export async function getStaticPaths() {
  const categories = await getCategories();
  const paths = (await Promise.all(categories.flatMap(async (category) => {
    const posts = await getCategory(category);
    return posts.map(post => ({params: {category, post: post.attributes.title.split(' ').join('-')}}));
  }))).flat();

  return {
    paths,
    fallback: false,
  }
}