import Layout from '../../components/Layout'
import ContentList from '../../components/ContentList'

const Index = ({ posts, category, categories }) => {
  return (
    <Layout siteTitle={`synaestheory.online`} scroll={true} categories={categories} >
      <ContentList category={category} posts={posts} />
    </Layout>
  )
}

export default Index

import { getCategories, getCategory } from '../../utils/getCategories'

export async function getStaticProps({params: {category}}) {
  const categories = await getCategories();
  const posts = await getCategory(category);

  return {
    props: { posts, category, categories }
  }
}

export async function getStaticPaths() {
  const categories = await getCategories();
  const paths = categories.map(category => ({params: {category}}));
  return {
    paths,
    fallback: false,
  }
}