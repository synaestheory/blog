import { InstagramEmbed } from '../components/InstagramEmbed';
import Layout from '../components/Layout'
import {attributes, react as ReactMarkdown} from '../data/info.md'
import { getCategories } from '../utils/getCategories';
export default function Info({categories}) { 
  return (
    <Layout siteTitle={attributes.title} categories={categories}>
      <section className="info_blurb">
        <ReactMarkdown />
      </section>
      <InstagramEmbed />
      <style jsx>{`
        .info_blurb {
          max-width: 800px;
          padding: 1.5rem 1.25rem;
        }

        @media (min-width: 768px) {
          .info_blurb {
            padding: 2rem;
          }
        }

        @media (min-width: 1440px) {
          .info_blurb {
            padding: 3rem;
          }
        }
      `}</style>
    </Layout>
  )
}

export async function getStaticProps() {
  const categories = await getCategories();
  return {
    props: { categories }
  }
}
