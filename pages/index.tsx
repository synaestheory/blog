import Layout from '../components/Layout'
import FullscreenIframe from '../components/Post'
import { getCategories } from '../utils/getCategories'
import {attributes} from '../data/index.md';
const Index = ({categories}) => {
  return (
    <Layout siteTitle={attributes.title} scroll={true} categories={categories}>
      <FullscreenIframe external_link={attributes.external_link} />
    </Layout>
  )
}

export default Index

export async function getStaticProps() {
  const categories = await getCategories();
  return {
    props: { categories }
  }
}
