import React from 'react'
import App from 'next/app'
import Script from 'next/script'

class MyApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <>
        <Script defer src='https://unpkg.com/inobounce@0.2.0/inobounce.js'></Script>
        <Script defer src='https://static.cloudflareinsights.com/beacon.min.js' data-cf-beacon='{"token": "b56a538463f1411fb13b620accbe45a4"}'></Script>
        <Component {...pageProps} />
      </>
    )
  }
}


export default MyApp;
